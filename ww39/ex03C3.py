prompt = input("Please enter score ")
try:
    score = float(prompt)
except:
    print("Please enter a numerical value")
    quit()

print(score)
if score < 0.6:
    print("F")
elif 0.60 <= score <= 0.69:
    print("D")
elif 0.70 <= score <= 0.79:
    print("C")
elif 0.80 <= score <= 0.89:
    print("B")
elif 0.90 <= score <= 0.99:
    print("A")
else:
    print("Bad score")

