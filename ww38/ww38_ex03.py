"""
Exercise 3 - Python for everybody chapter 2 exercises


Exercise 1: Type the following statements in the Python interpreter to
see what they do:


Exercise 2: Write a program that uses input to prompt a user for their
name and then welcomes them.

name = input("What is your name?")
print ('Welcome', name)


Exercise 3: Write a program to prompt the user for hours and rate per
hour to compute gross pay.

hours = input("Please enter hours.")
rate = input("Please enter rate.")
pay = float(hours) * float(rate)
print("Pay:")
print(pay)



Exercise 4: Assume that we execute the following assignment statements:

width = 17
height = 12.0

For each of the following expressions, write the value of the expression and the
type (of the value of the expression).
1. width//2 #quotient = 8 (8*2 = 16) Remainder: 1 , type: int.
2. width/2.0 #division = 8,5 (17/2) , type: float.
3. height/3 #division = 4 (12/4) , type: int
4. 1 + 2 * 5 #multiplying, then addding: 2*5 + 1 = 11 , type: int


Exercise 5: Write a program which prompts the user for a Celsius temperature, convert the temperature to Fahrenheit, and print out the
converted temperature.


celsius = (input("Please enter a degree in celsius "))
fahrenheit = (float(celsius) * 9) / 5 + 32
print("Your temperature in fahrenheit is:", fahrenheit)


°C to °F   Divide by 5, then multiply by 9, then add 32
°F to °C   Deduct 32, then multiply by 5, then divide by 9


"""