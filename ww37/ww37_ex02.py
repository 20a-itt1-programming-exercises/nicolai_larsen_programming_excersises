"""
Exercise 1: What is the function of the secondary memory in a computer?
c) Store information for the long term, even beyond a power cycle


Exercise 2: What is a program?
A: A collection of tasks that is created by the developer. (A set of instructions that specifies a computation)


Exercise 3: What is the difference between a compiler and an interpreter?
A: An Anterpreter reads 1 line of code at a time, where the compiler reads the entire file


Exercise 4: Which of the following contains “machine code”?

c) Python source file
When you installed Python on your computer, you copied a machine-code copy of the translated Python
program onto your system. 

Exercise 5: What is wrong with the following code: 

1.14. EXERCISES 17
>>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'
^
SyntaxError: invalid syntax
>>>

A: It should say "Print" and not "Primt", since that is not the correct syntax for that given task.

Exercise 6: Where in the computer is a variable such as “x” stored after
the following Python line finishes?
x = 123
b) Main Memory
There is no point of storing x in the secondary memory, hence it should be main memory.


Exercise 7: What will the following program print out:
x = 43
x = x + 1
print(x)
b) 44 


Exercise 8: Explain each of the following using an example of a human capability:
(1) Central processing unit(your brain that thinks),
(2) Main Memory(Nervous system?), the part of the brain that makes your legs move automatically when u walk, breathing etc.))
(3) Secondary Memory(Long team memory, your mind, things u remember)
(4) Input Device(Read) - Your vision/hearing,smell
(5) Output Device(Write) - Speech, movement

Exercise 9: How do you fix a “Syntax Error”?
A: Compare it to a normal language, where u have a gramatically/spelling error. U just have to put the right letter/icon, in order
to fix a syntax error. I.E look at excercise 5. 
"""
